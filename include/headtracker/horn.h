/**
 * @file:horn.hpp
 * @author: Prerna Gera
 */
#ifndef HEADTRACKER_HORN_HPP
#define HEADTRACKER_HORN_HPP
#include <Eigen/Eigen>
#include<iostream>
#include<Eigen/Dense>
#include <vector>

/**
 * COMPUTES MEAN of 3D points
 * @param v: std::vector of 3D points
 */
std::vector<double> mean(std::vector<std::vector<double> > v);


/**
 * To compute Covariance Matrix
 * @param a, b: std::vector of 3D points
 */
Eigen::MatrixXf covariance(std::vector<std::vector<double> > a, std::vector<std::vector<double> > b);

/**
 * Compute rigid transformation matrix given two set of 3D points.
 *
 * @param x, y: std::vectors of 3d points
 * @return : 4x4 homogeneous transformation matrix
 */
Eigen::Matrix4f horn(std::vector<std::vector<double> > x,std::vector<std::vector<double> >y );

#endif
