/**
 * @author: Vikas Dhiman
 */
#ifndef HEADTRACKER_IO_H
#define HEADTRACKER_IO_H
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/thread.hpp>

#include <pcl/io/openni_camera/openni_depth_image.h>
#include <pcl/io/openni_camera/openni_image.h>
#include <pcl/io/openni_grabber.h>

#include <opencv2/opencv.hpp>

/**
 * Queries RGB and depth image from Asus Xtion/Kinect using Point cloud
 * library.
 *
 * Point cloud library (OpenNIGrabber) provides a callback interface to
 * capturing depth and rgb images. While working with VTK like gui, that has
 * it's own callback interface, we need some mechanism to cache the latest
 * images so that those can be queried on request. This class provides that
 * mechanism.
 *  
 * Example usage:
 *
 *    RGBD_IO r();
 *    r.run();
 *    // wait for IO to be ready for first image.
 *    while (!r.is_ready()) {
 *	sleep(1);
 *    }
 *    cv::Mat image, depth;
 *    r.getImageAndDepth(&image, &depth);
 *    // Use your image and depth here
 *    r.shutdown();
 */
class RGBD_IO {
  private:
    // To save latest RGB and depth image
    boost::shared_ptr<openni_wrapper::Image> lastRGBImage_;
    boost::shared_ptr<openni_wrapper::DepthImage> lastDepthImage_;
    // For locking the RGB and depth image
    boost::mutex guard_;
    // For signaling shutdown to background thread
    bool is_shutdown_;
    // For did we get our first pair of images so far.
    bool is_ready_;

  protected:
    void image_depth_cb_ (
	const boost::shared_ptr<openni_wrapper::Image> &image,
	const boost::shared_ptr<openni_wrapper::DepthImage> &depth_image,
	float focallength);
    void run_in_thread(pcl::Grabber* interface);


  public:
    RGBD_IO() : lastDepthImage_(),
    lastRGBImage_(),
    is_shutdown_(false),
    is_ready_(false)
    {
      // empty constructor
    }

    /** Returns true when we have first image available */
    inline bool is_ready() { return is_ready_; }

    /** Query RGB and depth image */
    void getImageAndDepth(cv::Mat* image, cv::Mat* depth);

    /** Initialize RGB image of right size and type */
    cv::Mat initImage();

    /** Initialize Depth image of right size and type */
    cv::Mat initDepth();

    /** Spawns a thread to capture images from PCL's OpenNIGrabber */
    boost::function<void ()> run();

    /** Signals the thread to exit gracefully. Also called in destructor */
    inline void shutdown() { is_shutdown_ = true; };

    ~RGBD_IO() {
        shutdown();
    }
};
#endif
