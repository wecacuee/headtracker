/**
 * @author: Prerna Gera
 */
#include <headtracker/horn.h>
using std::vector;

int main()
{

// Create
    vector< vector<double> > vec1(2, vector<double>(3, 0));
    // Write
vec1[0][0] = 1;
vec1[0][1] =0;
vec1[0][2] = 2;

vec1[1][0] = 1;
vec1[1][1] =2;
vec1[1][2] = 0;
horn(vec1,vec1);
/****TEST*************************************/
//MatrixXf A(3,2);
//A(0,0) = 1;
//A(0,1) = 0;
//A(1,0) = 0;
//A(1,1) = 2;
//A(2,0)=0;
//A(2,1)=1;
//std::cout << "Here is the matrix A:\n" << A << std::endl;
/*
//TEST for mean function
// Create
    vector< vector<double> > vec(4, vector<double>(3, 0));
    // Write
vec[0][0] = 1;
vec[0][1] =2;
vec[0][2] = 3;

vec[1][0] = 2;
vec[1][1] =3;
vec[1][2] = 4;

vec[2][0] = 3;
vec[2][1] =4;
vec[2][2] = 1;	

vec[3][0] = 4;
vec[3][1] =1;
vec[3][2] = 2;

    // Read
   for (vector<vector<double> >::iterator i = vec.begin();
                           i != vec.end();
                           ++i)
{
   cout << (*i)[0] << ", " <<(*i)[1]<<","<<(*i)[2]  << endl;
}
 

vector<double> m= mean(vec);

   cout << m[0] << ", " <<m[1]<<","<<m[2]  << endl;

*////////////////////////////////////////////////////////////		
/*To test  covariance*/
/*
// Create
//    vector< vector<double> > vec1(2, vector<double>(3, 0));
    // Write
//vec1[0][0] = 1;
//vec1[0][1] =0;
//vec1[0][2] = 2;

//vec1[1][0] = 1;
//vec1[1][1] =2;
//vec1[1][2] = 0;
//vec1[2][0] = 3;
//vec1[2][1] =2;
//vec1[2][2] = 1;	

//vec1[3][0] = 4;
//vec1[3][1] =4;
//vec1[3][2] = 2;



    // Read
//   for (vector<vector<double> >::iterator i = vec1.begin();
  //                         i != vec1.end();
    //                       ++i)
//{
  // cout << (*i)[0] << ", " <<(*i)[1]<<","<<(*i)[2]  << endl;
//}
 */
/******************************TEST************************************************************************/
//MatrixXf C(3,3);
//C= covariance(vec1,vec1); //TEST

//   cout << m[0] << ", " <<m[1]<<","<<m[2]  << endl;


  // JacobiSVD<MatrixXf> svdOfA(C,ComputeFullU|ComputeFullV);      
   //const Eigen::MatrixXf U = svdOfA.matrixU();
   //const Eigen::MatrixXf V = svdOfA.matrixV();
   //const Eigen::VectorXf S = svdOfA.singularValues();
//   std::cout<<"Matrix U:\n" <<U<<std::endl;
 //  std::cout<<"Matrix V:\n" <<V<<std::endl;
  // std::cout<<"Matrix S:\n" <<S<<std::endl;      
   		 
   return 0;
}

