/**
 * Not used. See test/test_face_detection.cpp instead for a
 * working version of this code.
 *
 * @file: vis.cpp
 * @author: Vikas Dhiman
 */
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataReader.h>
#include <vtkActor.h>
#include <vtkCommand.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTransform.h>
#include <Eigen/Geometry>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
//#include "opencv2/core/utility.hpp"

#include "opencv2/highgui/highgui_c.h"

#include <iostream>
#include <stdio.h>

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

#include <headtracker/io.h>
#include <headtracker/objectDetection.h>
#include <headtracker/maskto3d.h>

// Rotate the vtkActor by given rigid transformation T
void rotateAndTranslate(vtkActor *actor, Eigen::Matrix4f T) {
    double elements[16];
    for (int r =0; r < 4; r ++ )
      for (int c=0; c < 4; c++ )
        elements[r*4 + c] = T(r, c);
    vtkSmartPointer<vtkTransform> transf = 
      vtkSmartPointer<vtkTransform>::New();
    transf->SetMatrix(elements);
    double* pos = transf->GetPosition();
    cout << "Position:" << pos[0] << " " << pos[1] << " " << pos[2] << endl;
    actor->SetPosition(transf->GetPosition());
    double* orientation = transf->GetOrientation();
    cout << "Orientation:" << orientation[0] << " " << orientation[1] << " "
      << orientation[2] << endl;
    actor->SetOrientation(transf->GetOrientation());
}
 
class vtkTimerCallback2 : public vtkCommand
{
  public:
    static vtkTimerCallback2 *New()
    {
      vtkTimerCallback2 *cb = new vtkTimerCallback2;
      cb->TimerCount = 0;
      return cb;
    }
 
    virtual void Execute(vtkObject *caller, unsigned long eventId,
                         void * vtkNotUsed(callData))
    {
      if (vtkCommand::TimerEvent == eventId)
        {
        ++this->TimerCount;
        }
      //std::cout << this->TimerCount << std::endl;
      cv::Mat frame = rgbd_io->initImage();
      cv::Mat depth = rgbd_io->initDepth();
      rgbd_io->getImageAndDepth(&frame, &depth);
      cv::cvtColor(frame,frame,CV_RGB2BGR);
      std::vector<Rect> faces_req;
      cv::imshow("input", frame);
      cv::waitKey(33);
      if( !frame.empty() ) {
          cout << "frame empty" << endl;
          return;
      }
      int maxind = detectAndTrack( face_cascade_name, &frame, &faces_req);
      if (maxind < 0) {
          cout << "face not found" << endl;
        return;
      }
      cv::Mat vis;
      frame.copyTo(vis);
      Rect face_rect = faces_req[maxind];

      Eigen::Matrix4f T = extractFeatures(
          frame, face_rect, depth, vis);

      cout << "Transformation:" << T << endl;
      rotateAndTranslate(actor, T);
      imshow("c", vis );
      //int c = waitKey(10);

      // get images and features and poses

      //rotateAndTranslate(actor);
      //actor->SetPosition(this->TimerCount, this->TimerCount,0);
      vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::SafeDownCast(caller);
      iren->GetRenderWindow()->Render();
    }
 
  private:
    int TimerCount;
  public:
    vtkActor* actor;
    RGBD_IO* rgbd_io;
    string face_cascade_name;
};
 
int main(int argc, char** argv)
{
  //if (show_animated_face) {
    vtkSmartPointer<vtkPolyDataReader> pdReader =
  vtkSmartPointer<vtkPolyDataReader>::New();
    pdReader->SetFileName(const_cast<const char*>(argv[1]));
    pdReader->Update();
  // } else {
  //     vtkSmartPointer<vtkPoints> points = 
  //       vtkSmartPointer<vtkPoints>::New();

  //     vtkSmartPointer<vtkPolyData> polydata =
  //       vtkSmartPointer<vtkPolyData>::New();
  //     polydata.SetPoints(points)
  // }


  //vtkSmartPointer<vtkPolyData> 
  // Create a sphere
  // vtkSmartPointer<vtkSphereSource> sphereSource = 
  //   vtkSmartPointer<vtkSphereSource>::New();
  // sphereSource->SetCenter(0.0, 0.0, 0.0);
  // sphereSource->SetRadius(5.0);
  // sphereSource->Update();
 
  // Create a mapper and actor
  vtkSmartPointer<vtkPolyDataMapper> mapper = 
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(pdReader->GetOutputPort());
  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
 
  // Create a renderer, render window, and interactor
  vtkSmartPointer<vtkRenderer> renderer = 
    vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = 
    vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = 
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);
 
  // Add the actor to the scene
  renderer->AddActor(actor);
  renderer->SetBackground(1,1,1); // Background color white
 
  // Render and interact
  renderWindow->Render();
 
  // Initialize must be called prior to creating timer events.
  renderWindowInteractor->Initialize();


  // Initialize the system
  //
  string face_cascade_name = argv[2];
  RGBD_IO v;
  boost::function<void ()> workerfun = v.run();
  boost::thread workerthread(workerfun);
  while (!v.is_ready()) {
      sleep(1);
  }
 
  // Sign up to receive TimerEvent
  vtkSmartPointer<vtkTimerCallback2> cb = 
    vtkSmartPointer<vtkTimerCallback2>::New();
  cb->actor = actor;
  cb->rgbd_io = &v;
  cb->face_cascade_name = face_cascade_name;
  renderWindowInteractor->AddObserver(vtkCommand::TimerEvent, cb);
 
  int timerId = renderWindowInteractor->CreateRepeatingTimer(100);
  //std::cout << "timerId: " << timerId << std::endl;
 
  // Start the interaction and timer
  renderWindowInteractor->Start();
 
  return EXIT_SUCCESS;
}
