/**
 * @author: Vikas Dhiman
 */
#include <unistd.h>
#include <pcl/io/openni_grabber.h>
#include <opencv2/opencv.hpp>

#include <headtracker/io.h>
#include <boost/thread.hpp>
using std::cout;
using std::endl;

/******************************************************************************/
void RGBD_IO::run_in_thread(pcl::Grabber* interface) {
    // thread 1
    interface->start ();
    while (!is_shutdown_) {
        usleep(33000);
    }

    interface->stop ();
}

/******************************************************************************/
boost::function<void ()>
RGBD_IO::run() {
    // thread 1
    pcl::Grabber* interface = new pcl::OpenNIGrabber ();

    // bind this object with callback
    boost::function<void
      (const boost::shared_ptr<openni_wrapper::Image> &image,
       const boost::shared_ptr<openni_wrapper::DepthImage> &depth_image,
       float focallength) >
      f = boost::bind(&RGBD_IO::image_depth_cb_, this, _1, _2, _3);

    // and register the callback
    interface->registerCallback (f);

    boost::function<void ()> threadfun = boost::bind(&RGBD_IO::run_in_thread, this, interface);
    return threadfun;
}

/******************************************************************************/
void RGBD_IO::image_depth_cb_ (
    const boost::shared_ptr<openni_wrapper::Image> &image,
    const boost::shared_ptr<openni_wrapper::DepthImage> &depth_image,
    float focallength)
{
  //cout << "I am ready" << endl;
  // thread 1
  // There are two threads thread 1 we write images to these shared variables,
  // while in thread 2 we return them on request. This is thread 1.
  boost::mutex::scoped_lock lock(guard_);
  is_ready_ = true;
  //lastRGBImage_ = RGBD_IO::getFrame(image);
  lastRGBImage_ = image;
  //lastDepthImage_ = RGBD_IO::getDepthFrame(depth_image);
  lastDepthImage_ = depth_image;
}

// cv::Mat
// RGBD_IO::getFrame (const
// boost::shared_ptr<openni_wrapper::Image> &img)
// {
//   cv::Mat frameRGB=cv::Mat(img->getHeight(),img->getWidth(),CV_8UC3);
// 
//   img->fillRGB(frameRGB.cols,frameRGB.rows,frameRGB.data,frameRGB.step);
//   cv::cvtColor(frameRGB,frameRGB,CV_RGB2BGR);
// 
//   return frameRGB;
// }

// cv::Mat
// RGBD_IO::getDepthFrame(
// 	const boost::shared_ptr<openni_wrapper::DepthImage> &depth_image)
// {
//   cv::Mat frameDepth=cv::Mat(depth_image->getHeight(),depth_image->getWidth(),CV_32F);
//   depth_image->fillDepthImage(frameDepth.cols,frameDepth.rows,(float *)frameDepth.data,frameDepth.step);
//   return frameDepth;
// }

/******************************************************************************/
cv::Mat RGBD_IO::initImage() {
    cv::Mat frameRGB=cv::Mat(lastRGBImage_->getHeight(), lastRGBImage_->getWidth(),CV_8UC3);
    return frameRGB;
    //return lastRGBImage_.clone();
    // for speed
    //return lastRGBImage_;
}

/******************************************************************************/
cv::Mat RGBD_IO::initDepth() {
    cv::Mat frameDepth=cv::Mat(lastDepthImage_->getHeight(),lastDepthImage_->getWidth(),CV_32F);
    return frameDepth;
    //return lastDepthImage_.clone();
    // for speed
    //return lastDepthImage_;
}

/******************************************************************************/
void
RGBD_IO::getImageAndDepth(cv::Mat* frameRGB, cv::Mat* frameDepth) {
    // There are two threads thread 1 we write images to these shared variables,
    // while in thread 2 we return them on request. This is thread 2.
    boost::mutex::scoped_lock lock(guard_);
    lastRGBImage_->fillRGB(frameRGB->cols,frameRGB->rows,frameRGB->data,frameRGB->step);
    //cv::cvtColor(*frameRGB,*frameRGB,CV_RGB2BGR);

    // cv::Mat frameRGB = getImage();
    // lastRGBImage_->fillRGB(frameRGB.cols,frameRGB.rows,frameRGB.data,frameRGB.step);
    // cv::cvtColor(frameRGB, *frameBGR,CV_RGB2BGR);
    //lastRGBImage_.copyTo(*image);
    lastDepthImage_->fillDepthImage(frameDepth->cols,frameDepth->rows,(float *)frameDepth->data,frameDepth->step);
    //lastDepthImage_.copyTo(*depth);
}

